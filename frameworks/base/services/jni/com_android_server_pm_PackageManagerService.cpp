#define LOG_TAG "PMS-JNI"

//#define LOG_NDEBUG 0

#include "JNIHelp.h"
#include "jni.h"

#include <limits.h>
#include "utils/Log.h"
#include <sys/syscall.h>
#include <android_runtime/AndroidRuntime.h>
#include "Permission.h"

/* COMPAC_PMS_JNI */
namespace android {

// ----------------------------------------------------------------------------

void android_server_pm_PackageManagerService_addToKernelPolicyManager(JNIEnv *env, jobject clazz, jint uid, jstring packageName, jobjectArray restrictedpermissions) {
	char pkgpermissions[4][100];
	memset(pkgpermissions,0,400);
	char pkgName[100];
	strcpy(pkgName, (*env).GetStringUTFChars(packageName, 0));
	int i=0;
	jstring permission;
	jint size = (*env).GetArrayLength(restrictedpermissions);	
	for(i=0;i<size;i++) {
		permission = (jstring) (*env).GetObjectArrayElement(restrictedpermissions, i);		
		strcpy(pkgpermissions[i],(*env).GetStringUTFChars(permission, NULL));
	}
	
	//Syscall to kernel slave policy manager	
	syscall(CONTROLPERM, uid, pkgName, 100, pkgpermissions, 400, 1);
}

//-----------------------------------------------------------------------------

static jlong android_server_pm_PackageManagerService_getperm(JNIEnv *env, jobject clazz, jint tid, jstring permission, jint control) {

	const char *c_permission = env->GetStringUTFChars(permission, NULL);
//	LOGE("COMPAC getperm=%s", c_permission);
	long permToken = syscall(GETPERM, tid, c_permission, 0);
	env->ReleaseStringUTFChars(permission, c_permission);
	return (jlong) permToken;

	//return 0;
}
// ----------------------------------------------------------------------------

static JNINativeMethod gPackageManagerServiceMethods[] = {
    /* name, signature, funcPtr */
{ "addToKernelPolicyManager" , "(ILjava/lang/String;[Ljava/lang/String;)V", (void*) android_server_pm_PackageManagerService_addToKernelPolicyManager },
    { "getperm", "(ILjava/lang/String;I)J", (void*) android_server_pm_PackageManagerService_getperm },
};

int register_android_server_pm_PackageManagerService(JNIEnv* env) {
    return jniRegisterNativeMethods(env, "com/android/server/pm/PackageManagerService",
            gPackageManagerServiceMethods, NELEM(gPackageManagerServiceMethods));

}

} /* namespace android */
