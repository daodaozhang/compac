/* COMPAC only */
package android.content.pm;

import java.util.HashMap;
import java.util.HashSet;

import android.content.pm.PackageManager;
import android.util.Slog;

public class PermissionGuard {
	private static final String TAG = "COMPAC_COMPONENT_PERMISSION";
	String tid;
	public HashMap <String, HashSet<String>> packagePermissionGuard = new HashMap <String, HashSet<String>>();	
	
	public void addPermission(String packageName, String permissionName) {
		Slog.v(TAG, "add Permission: " + permissionName + " to " + packageName);
		HashSet<String> permissionSet = this.packagePermissionGuard.get(packageName);
		if(permissionSet != null){
			permissionSet.add(permissionName);
		}  else {
			permissionSet = new HashSet<String>();
			permissionSet.add(permissionName);
			this.packagePermissionGuard.put(packageName, permissionSet);
		}
	}
	
	public void addPermissionSet(String packageName, HashSet<String> pkgPermissions) {
		Slog.v(TAG, "add PermissionSet: " + packageName + " to " + packageName);
		HashSet<String> permGuard = this.packagePermissionGuard.get(packageName);		
		if(permGuard == null){
			this.packagePermissionGuard.put(packageName, pkgPermissions);
		}  
	}
	
	public int comparePackagePermission(String tid, String packageName, String permissionName) {
		if(this.packagePermissionGuard.containsKey(packageName)) {
			if(this.packagePermissionGuard.get(packageName).contains(permissionName)) {
				return PackageManager.PERMISSION_GRANTED;
			}
		} 
		return PackageManager.PERMISSION_DENIED;		
	}
}
