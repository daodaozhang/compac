/* COMPAC */
package android.content.pm;

import java.util.HashMap;
import java.util.HashSet;

import android.util.Slog;

public class PackagePolicyManager {
	
	HashMap<String,PermissionGuard> appPackagePermissions = new HashMap<String,PermissionGuard>();

	public PermissionGuard getPermissionGuard(String uid) {
		return appPackagePermissions.get(uid);		
	}
	
	/*public void addPermissionGuard_Set(String uid, String tid, String packageName, HashSet<String> pkgPermissionRestrictions){
		PermissionGuard permGuard = getPermissionGuard(uid);
		if(permGuard!=null) {
			permGuard.addPermissionSet(packageName, pkgPermissionRestrictions);
		} else {
			permGuard = new PermissionGuard(tid);
			permGuard.addPermissionSet(packageName, pkgPermissionRestrictions);
			appPackagePermissions.put(uid, permGuard);
		}		
	}*/	
	
	public void deleteUidPolicy(String uid) {		
		appPackagePermissions.remove(uid);		
	}
	
	/*public void replaceKeybyUid(String uid, String appName) {
		PermissionGuard pGuard = appPackagePermissions.get(appName);
		if(pGuard!=null) {
			appPackagePermissions.put(uid, pGuard);
			appPackagePermissions.remove(appName);
		} else
			Slog.v("COMPAC", "AppPermissons Map returned Null");		
	}*/
	
	public void addPermissionGuard(String uid, PermissionGuard pGuard) {				
		appPackagePermissions.put(uid, pGuard);	
	}
	
	public void addPermission(String uid, String packageName, String permissionName) {
		//Slog.v("COMPAC", "addPermission - UID: "+uid);
		PermissionGuard permGuard = getPermissionGuard(uid);
		if(permGuard!=null) {
			permGuard.addPermission(packageName, permissionName);
		} else {
			permGuard = new PermissionGuard();
			permGuard.addPermission(packageName, permissionName);
			appPackagePermissions.put(uid, permGuard);
		}
	}
	
	public int checkPackagePermission(String uid, String tid, String packageName, String permissionName) {
		PermissionGuard p = this.appPackagePermissions.get(uid);
		if(p==null) {
			Slog.v("COMPAC", "package-permission not found in Policy Manager for UID: "+uid);
			return PackageManager.PERMISSION_DENIED;
		}
		return p.comparePackagePermission(tid, packageName, permissionName);	
	}
}
