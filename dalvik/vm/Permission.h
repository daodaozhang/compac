#ifndef DALVIK_PERMISSION_H_
#define DALVIK_PERMISSION_H_
/* COMPAC
 *
 * The log TAG in dalvik is DALVIKHOOK
 *
 */
#define DROIDGUARD
#define DROIDGUARD_DEBUG

#define COMPAC
#define COMPAC_DEBUG

/* defined for syscall */
#define SETPERM 		188
#define GETPERM 		189
#define CONTROLPERM 		223

#define ADDPACKAGE		0
#define DELPACKAGE		1

#define PERM_LENGTH 		30
#define PLENGTH 		100
#define MAXPACKAGENUM		30
#define MAXPACKAGELEN		100

struct packagePermission
{
        char package_name[PLENGTH];
        long hash;
        struct xml_permission *x_permissions;
};

struct Permission {
	int counter; /* For counting */
	char* packageName;
	const Method* method;
	struct Permission* pre;
};

int threadPermInit(struct Thread* newThread, struct Thread* oldThread);

int policyPackageInit();

int setperm(struct Thread* td);

void method_hook_call(Thread* self, const Method* method,
		const Method* oldmethod, const char* hookname);

void method_hook_return(Thread* self,
		const Method* methodtoreturn, const Method* gotomethod,
		const char* hookname);

#endif
